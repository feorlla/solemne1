/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author GorillaSetups
 */
public class Calculadora {
    private int capital;
    private int interes;
    private int agnos;
    private int resutlado;
    /**
     * @return the capital
     */
    public int getCapital() {
        return capital;
    }

    /**
     * @param capital the capital to set
     */
    public void setCapital(int capital) {
        this.capital = capital;
    }

    /**
     * @return the interes
     */
    public int getInteres() {
        return interes;
    }

    /**
     * @param interes the interes to set
     */
    public void setInteres(int interes) {
        this.interes = interes;
    }

    /**
     * @return the agnos
     */
    public int getAgnos() {
        return agnos;
    }

    /**
     * @param agnos the agnos to set
     */
    public void setAgnos(int agnos) {
        this.agnos = agnos;
    }

    /**
     * @return the resutlado
     */
    public int getResutlado() {
        return resutlado;
    }

    /**
     * @param resutlado the resutlado to set
     */
    public void setResutlado(int resutlado) {
        this.resutlado = resutlado;
    }
    public void formula(){
        this.resutlado=this.getCapital()*this.getInteres()/100*this.getAgnos();
    }
}
